# The Quest Knowledge base

This is a collection of relevant information for corpus creators compiled as 
part of the [QUEST](https://www.slm.uni-hamburg.de/ifuu/forschung/forschungsprojekte/quest.html)
project.

The information is stored as [reStructuredText](https://docutils.sourceforge.io/docs/ref/rst/restructuredtext.html),
which can e.g. be converted to HTML files.

To convert files a [Makefile](https://en.wikipedia.org/wiki/Makefile) is provided which requires 
[sed](https://www.gnu.org/software/sed/) and [Pandoc](https://pandoc.org/).

Additional contributions and/or translations are welcome.

<!-- a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a-->