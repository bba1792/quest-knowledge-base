QUEST Knowledge Base
====================

Types of corpora
----------------

- Introduction
- Multilingual corpora
- Multimodal corpora

Media formats
-------------

- Sound recordings
- Video recordings

Annotation formats
------------------

- Introduction
- ELAN
- EXMARaLDA
- FOLKER
- FLEX

Quality control
---------------

- `HZSK Corpus services <corpus_sevices>`__

Certification
-------------

- `Resource types <resource_types>`__

Legal aspects
-------------

- `Best practices <best_practices>`__
- `Data protection <data_protection>`__
- `Copyright <copyright>`__
